#include "NetworkClient.h"

#include "..\imgui\imgui.h"

typedef std::vector<Connection>::iterator ConnIter;

NetworkClient::NetworkClient() :
	console(ImGui::SFML::GetConsole()) {
	socket_.setBlocking(false);
	is_connected_ = false;
	has_connection_timeout_ = true;
	needs_collision_response_ = false;
	server_port_ = 50002;
	server_address_ = "127.0.0.1";
	request_timer_.SetTimer(1000);
	request_timeout_.SetTimer(10000);
}

NetworkClient::~NetworkClient() {
	
}

void NetworkClient::Update(sf::Time delta_time) {
	frame_ += delta_time.asMilliseconds();
	ProcessRecievedPackets();
	if (!is_connected_&& !has_connection_timeout_) {
		request_timer_.Update(delta_time.asMilliseconds());
		request_timeout_.Update(delta_time.asMilliseconds());
		if (request_timeout_.IsFinished()) {
			request_timeout_.Reset();
			has_connection_timeout_ = true;
			console.AddLine("Connection Timed Out.");
		}else if (request_timer_.IsFinished()) {
			request_timer_.Reset();
			SendConnectionRequest();
		}

	}
}

void NetworkClient::RenderImGui(bool *open) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(300, 400);
	ImVec2 window_pos = ImVec2(
		0,
		20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if (!ImGui::Begin("Client Debug ", open, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}
	
	if (!is_connected_&&has_connection_timeout_) {
		*open = true;
		ImGui::Text("Not Connected...");
		static char username[255] = "Username";
		static char server_address[255] = "127.0.0.1";
		ImGui::InputText("Username", username, sizeof(username));
		ImGui::InputText("Server", server_address, sizeof(server_address));
		if (ImGui::Button("Connect to Server", ImVec2(200, 50))) {
			has_connection_timeout_ = false;
			current_state_.username = username;
			server_address_ = server_address;
			SendConnectionRequest();			
		}
	}
	if (!is_connected_ && !has_connection_timeout_) {
		ImGui::Text("Connecting...");
	}
	if (is_connected_) {		
		ImGui::Text("Connected");

		ImGui::Text("ID %d, %s", current_state_.player_id, current_state_.username.c_str());
		ImGui::Text("Network GameState - Connected %d", current_game_state_.array_size);
		// Drop down menus for all connected players, shows there current network state.
		for (size_t i = 0; i <  current_game_state_.array_size; i++) {
			PlayerState player = current_game_state_.player_states[i];
			if (ImGui::TreeNode((void*)(intptr_t)player.player_id, "ID %d, %s", player.player_id, player.username.c_str())) {
				ImGui::Text("Pos: X: %.0f,Y: %.0f", player.position.x, player.position.y);
				ImGui::Text("Dir: X: %.0f,Y: %.0f", player.direction.x, player.direction.y);
				ImGui::Text("Type: %s", PlayerTypeToString(player.player_type).c_str());
				ImGui::TreePop();
			}
		}
		
		ImGui::Text("Game Running: %s", current_game_state_.game_running ? "true" : "false");
		ImGui::Text("ServerFrame: %d", current_game_state_.server_frame);
		ImGui::Text("Frame: %d", frame_);
		
	}
	

	ImGui::End();
}

bool NetworkClient::IsConnected() {
	return is_connected_;
}

bool NetworkClient::IsGameRunning() {
	return current_game_state_.game_running;
}

GameState & NetworkClient::GetNetworkState() {
	return current_game_state_;
}

PlayerState & NetworkClient::GetClientState() {
	return current_state_;
}

sf::Uint64 NetworkClient::Frame() {
	return frame_;
}

void NetworkClient::ProcessRecievedPackets() {
	sf::Packet recieved_packet;
	sf::IpAddress sender;
	unsigned short sender_port;
	MessageType message_type;
	if (socket_.receive(recieved_packet, sender, sender_port) != sf::Socket::Done) {
		return;
	}
	recieved_packet >> message_type;
	switch (message_type) {
		case kConnectionResponse:
		{
			ConnectionResponse conn_resp;
			recieved_packet >> conn_resp;
			
			is_connected_ = conn_resp.is_connected;
			if (is_connected_) {
				current_state_ = conn_resp.new_player_state;
				frame_ = conn_resp.server_frame;
				console.AddLine("Connected");
			}
			else {
				request_timeout_ = true;
				console.AddLine("Connection Denied");
			}
			
			
			break;
		}
		case kCollisionResponse:
		{
			console.AddLine("Collision Response");

			break;
		}
		case kGameEnd:
		{
			console.AddLine("Game End");

			break;
		}
		case kGameState:
		{
			//console.AddLine("New Game State");

			GameState new_game_state;
			recieved_packet >> new_game_state;
			// Only process it if it is a new update.
			if (new_game_state.server_frame > current_game_state_.server_frame) {
				
				current_game_state_ = new_game_state;
				// check if clients state is same as inbound game state.
				if (new_game_state.player_states[current_state_.player_id].last_frame < current_state_.last_frame) {
					// inbound isnt uptodate, resend a player update.
					SendPlayerUpdate(true);
				}				
				
			}
			break;
		}
		default:
			console.AddLine("Packet Corrupt");
			break;
	}

}

void NetworkClient::SendConnectionRequest() {
	console.AddLine("Connection Request Sent");
	ConnectionRequest conn_req;
	conn_req.username = current_state_.username;
	sf::Packet send_packet;
	send_packet << (sf::Uint8)MessageType::kConnectionRequest;
	send_packet << conn_req;
	socket_.send(send_packet,server_address_, server_port_);
}

void NetworkClient::SendPlayerUpdate(bool is_resend) {
	console.AddLine("Player Update Sent");
	PlayerUpdate player_update;
	if (!is_resend) {
		current_state_.last_frame = frame_;
	}
	player_update.new_player_state = current_state_;

	current_game_state_.player_states[current_state_.player_id] = current_state_;

	sf::Packet send_packet;
	send_packet << (sf::Uint8)MessageType::kPlayerUpdate;
	send_packet << player_update;
	socket_.send(send_packet, server_address_, server_port_);
}

void NetworkClient::SendPlayerCollision() {
}

