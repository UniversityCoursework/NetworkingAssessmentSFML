#include "ClientApplication.h"

#include "..\ImGui\imgui.h"

#include "..\Tag-Game\ControlledPlayer.h"

#include "..\MathLibrary\VectorManipulation.h"

const sf::Uint64 way_too_much_lag = 20000;

static const sf::IntRect texture_rects[] = {
	sf::IntRect(0,0,152,168),	// wallet
	sf::IntRect(0,0,200,120),	// money
};

ClientApplication::ClientApplication(NetworkClient& network_client, sf::RenderWindow & window)
	:BaseApplication(window), client_(network_client), game_state_(network_client.GetNetworkState()), player_state_(network_client.GetClientState()), console(ImGui::SFML::GetConsole()) {
	wallet_.loadFromFile("../res/Wallet.png");
	money_.loadFromFile("../res/Money.png");


}

ClientApplication::~ClientApplication() {
	BaseApplication::~BaseApplication();
}

void ClientApplication::Init() {

	for each (PlayerState new_state in game_state_.player_states) {
		Player* new_player;
		if(player_state_.player_id == new_state.player_id) {
			new_player = new ControlledPlayer(window_);			
		}
		else {
			new_player = new Player(window_);			
		}
		switch(new_state.player_type) {
			case PlayerType::kWallet:
			{
				new_player->Initialise(new_state, wallet_, texture_rects[0]);
				break;
			}
			case PlayerType::kMoney:
			{
				new_player->Initialise(new_state, money_, texture_rects[1]);
				break;
			}
			default:
				break;
		}
		players_.push_back(new_player);
	}

	level_map_->Initialise();

}

void ClientApplication::NetworkUpdate(sf::Uint64 frame) {	
	int i = 0;
	for each (Player* player in players_) {
		if(i != player_state_.player_id) {
			
			sf::Uint64 diff_in_frame = frame - game_state_.player_states[i].last_frame;
			if(diff_in_frame < way_too_much_lag) {
				sf::Vector2f predicted_position = game_state_.player_states[i].position;
				// Calculate Predicted Player Position from playerstates
				// pos+direction*(frame-server_frame)
				predicted_position += game_state_.player_states[i].direction * player->GetMoveSpeed()*(diff_in_frame / 10000.f);

				player->SetPosition(LittleLot::Lerp(player->Position(), predicted_position, 0.001f));

			}

			// Check if its moving, if its not then move it closer to its resting position. 
			// Using a basic Lerp.
			if(game_state_.player_states[i].direction.x == 0 && game_state_.player_states[i].direction.y == 0) {
				player->SetPosition(LittleLot::Lerp(player->Position(), game_state_.player_states[i].position, 0.1f));
			}
			
			player->SetDirection(game_state_.player_states[i].direction);
		}
		else {
			game_state_.player_states[i].position = player->Position();
			game_state_.player_states[i].direction = player->Direction();
		}
		i++;
	}
}

void ClientApplication::Update(float delta_time) {
	// Centre on Player
	view_ = sf::View(sf::FloatRect(players_[player_state_.player_id]->Position().x - static_cast<float>(window_.getSize().x / 2),
								   players_[player_state_.player_id]->Position().y - static_cast<float>(window_.getSize().y / 2),
								   static_cast<float>(window_.getSize().x),
								   static_cast<float>(window_.getSize().y)));
	window_.setView(view_);

	int i = 0;
	for each (Player* player in players_) {
		// Check if player has changed direction.
		if(player->Update(delta_time)) {
			// If its the player update the server with the updated state.
			if(i == player_state_.player_id) {
				player_state_.position = player->Position();
				player_state_.direction = player->Direction();
				client_.SendPlayerUpdate();
			}
		}
		// If it is the controlled player.
		if(i == player_state_.player_id) {
			// Check if player has collided, and auto correct to touching.
			if(level_map_->CollisionCorrection(*player)) {			
				// Stop player moving.
				player->SetDirection(sf::Vector2f(0, 0));
				// Send update to server.
				player_state_.position = player->Position();
				player_state_.direction = player->Direction();
				// increment to show its different. (is in miliseconds so almost guarented next update will be newer).
				player_state_.last_frame += 1;
				client_.SendPlayerUpdate(true);
			}
		}
		i++;	
		
	}
}


void ClientApplication::RenderImGui(bool *open) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(200, 400);
	ImVec2 window_pos = ImVec2(
		window_.getSize().x - window_size.x,
		20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if(!ImGui::Begin("Game App", open, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}
	ImGui::Text("ID %d, %s",
				game_state_.player_states[player_state_.player_id].player_id,
				game_state_.player_states[player_state_.player_id].username.c_str());


	ImGui::End();
}
