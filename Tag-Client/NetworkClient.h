#ifndef _NETWORK_SERVER_H
#define _NETWORK_SERVER_H
#include "..\ImGui\imgui-SFML-console.h"

#include <vector>

#include "..\NetworkProtocol\Connection.h"
#include "..\NetworkProtocol\Messages.h"

#include "..\NetworkProtocol\Timer.h"

/// <summary>
/// Client for handling the client.
/// Handles recieveing packets from the server, connecting to the server, and ensure the server is uptodate with correct player state.
/// </summary>
class NetworkClient {
public:
	/// <summary>
	/// Sets up the timers, and default server values.
	/// </summary>
	NetworkClient();
	~NetworkClient();

	/// <summary>
	/// Handles proccessing recieved packets for that frame and server connection logic and timers.
	/// </summary>
	/// <param name="delta_time"></param>
	void Update(sf::Time delta_time);

	/// <summary>
	/// First displays the connection status with the server, and expose the ability to change username and server address.
	/// Displays the Network state of all the players in a drop down menu in an ImGui window.
	/// </summary>
	/// <param name="open"> Allows the window to be closed with a X in top corner. 
	/// Determine to render based on the bool passed in. </param>
	void RenderImGui(bool *open);

	bool IsConnected();
	bool IsGameRunning();

	GameState& GetNetworkState();
	PlayerState& GetClientState();
	sf::Uint64 Frame();

	/// <summary>
	/// Sends player  current game state to the server
	/// </summary>
	/// <param name="is_resend"> Whether it is just being recent due to lag/packet drop, doesn't update frame sent. </param>
	void SendPlayerUpdate(bool is_resend = false);

	
	/// <summary>
	/// Send message to the server Informing them that there has been a collision.
	/// </summary>	
	void SendPlayerCollision();

private:
	/// <summary>
	/// Proccess recieved packets in a non-blocking manor.
	/// </summary>
	void ProcessRecievedPackets();

	/// <summary>
	/// Send a request to the server requesting to connect.
	/// </summary>
	void SendConnectionRequest();

	bool is_connected_;
	bool has_connection_timeout_;
	bool needs_collision_response_;

	sf::UdpSocket socket_;

	PlayerState current_state_;

	GameState current_game_state_;

	ImGui::SFML::Console& console;
	Timer request_timer_;
	Timer request_timeout_;
	unsigned short server_port_;
	sf::IpAddress server_address_;
	sf::Uint64 frame_;
};

#endif
