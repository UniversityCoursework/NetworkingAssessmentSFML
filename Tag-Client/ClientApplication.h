#ifndef _CLIENT_APPLICATION_H

#define _CLIENT_APPLICATION_H

#include "..\Tag-Game\BaseApplication.h"
#include "NetworkClient.h"

/// <summary>
/// Handles displaying a local client Game State. Derived from <seealso cref="BaseApplication"/>
/// 
/// Follows the clients player, allowing them to control them and change direction etc.
/// Handles interpolation and prediction of Network server game state as it comes in.
/// </summary>
class ClientApplication :public BaseApplication {
public:
	/// <summary>
	/// Ensures Renderwindow refrence bound correctly, and refrence to network client is bound correctly.
	/// </summary>
	/// <param name="network_client"> Client used to access network state, and update if needs be. </param>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	ClientApplication(NetworkClient& network_client, sf::RenderWindow& window);
	~ClientApplication();

	/// <summary>
	/// Sets up all the players based on network gamestate.
	/// Sets up the level map as well.
	/// Setting the controlled player up.
	/// </summary>
	void Init() override;

	/// <summary>	
	/// Sets players positions, and handles interpolation and prediction of Network server game state as it comes in.
	/// 
	/// Update the client data to closely match the server, and correct errors, caused by packet drop or high latency.
	/// </summary>
	/// <param name="frame"> Current frame of the app.</param>
	void NetworkUpdate(sf::Uint64 frame) override;

	/// <summary>
	/// Update all players for collision, Centres the view on the player.
	/// Updates the clients player based on user input.
	/// </summary>
	/// <param name="delta_time"> Time in seconds since last update. </param>
	void Update(float delta_time) override;

	/// <summary>
	/// Displays the client username and ID.
	/// Exposes controls to change selected player.	
	/// </summary>
	/// <param name="open"> Allows the window to be closed with a X in top corner. 
	/// Determine to render based on the bool passed in. </param>
	void RenderImGui(bool *open) override;
private:
	NetworkClient& client_;
	
	GameState& game_state_;
	PlayerState& player_state_;
	ImGui::SFML::Console& console;
};

#endif // !_SERVER_APPLICATION_H

