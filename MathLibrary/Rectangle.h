#ifndef _RECTANGLE_H
#define _RECTANGLE_H


#include <SFML/Graphics.hpp>

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	///<summary> Hit Struct gives information about the collision. 
	///<para> Delta value is the amount to remove from an objects position to set it as touching, from after a collision test.</para>
	///</summary>
	struct Hit {
		Hit() :position(sf::Vector2f()), delta(sf::Vector2f()), normal(sf::Vector2f()), time(0) {}
		sf::Vector2f position;
		sf::Vector2f delta;
		sf::Vector2f normal;
		float time;
	};

	///<summary> Rectangle class used for simplified collision detection. </summary>
	class Rectangle {
	public:
		Rectangle();		
		/// <summary>
		/// Generates a rectangle from the provided SFML FloatRect.
		/// </summary>
		/// <param name="float_rect">
		/// Best to use the FloatRect from Sprite getGlobalBounds. 
		/// As ensures all transforms,scales etc. are in effect. 
		/// </param>
		Rectangle(sf::FloatRect float_rect);
					
		/// <summary>
		/// Creates Rectangle centred at the given x/y, with half width/half height.
		/// 
		/// Remember x,y should be the centre of the object.
		/// </summary>
		/// <param name="x"> Centre X Component. </param>
		/// <param name="y"> Centre Y Component. </param>
		/// <param name="half_width"> Half the overall width of the rectangle. </param>
		/// <param name="half_height"> Half the overall height of the rectangle. </param>
		Rectangle(float x, float y, float half_width, float half_height);

		~Rectangle() {};

		///<summary>  </summary>
		/// <summary>
		/// Simple Broad Phase checks if there is any intersection between the 2 rectangles.
		/// </summary>
		/// <param name="rect"> The other rect to check collision against.</param>
		/// <returns> True if there is a collision.</returns>
		const bool IntersectsWith(const Rectangle &rect) const;
						
		/// <summary>
		/// Checks if the rectangle is fully within the passed in rect.
		/// 
		/// Returns true if the Rectangle is within the passed in rect.
		/// </summary>
		/// <param name="rect"> The other rect to check collision against.</param>
		/// <returns> True if there is a collision. </returns>
		bool WithinRect(const Rectangle &rect);
				
		/// <summary>
		/// Rectangle to rectangle collision checks the amount of overlap between the 2 rectangles.
		/// 
		/// Prefer the use of <seealso cref="IntersectsRectangle"/> as provides back more information.
		/// </summary>
		/// <param name="rect"> The other rect to check collision against.</param>
		/// <returns> The amount of overlap if the Rectangle is not within the passed in rect.</returns>
		sf::Vector2f WithinRectOverlap(const Rectangle &rect);

		
		/// <summary>
		/// Checks if the 2D point is withinthe Rectangle.
		/// 
		/// Returns true if the point is within the Rectangle.
		/// </summary>
		/// <param name="point"> 2D point in space the rect will be checked against.</param>
		/// <returns> True if the point is within the Rectangle.</returns>
		bool ContainsPoint(const sf::Vector2f &point);
		
		/// <summary>
		/// Checks the rectangle against a 2D point for collision.
		/// 
		/// Modifies the hit struct to provide normal and delta values of the collision.
		/// Returns true if there is a collision.
		/// </summary>
		/// <param name="hit"> Will be filled with collision info with relation to owning rect.</param>
		/// <param name="point"> 2D point in space the rect will be checked against. </param>
		/// <returns> True if there is a collision</returns>
		const bool IntersectsPoint(Hit &hit, const sf::Vector2f point) const;
				
		/// <summary>
		/// Narrower Phase Rectangle to rectangle collision check. 
		/// 
		/// Modifies the hit struct to provide normal and delta values of the collision.
		/// Returns true if there is a collision.
		/// </summary>
		/// <param name="hit"> Will be filled with collision info with relation to owning rect. </param>
		/// <param name="rect"> The other rect to check collision against. </param>
		/// <returns> True if there is a collision</returns>
		const bool IntersectsRectangle(Hit &hit, const Rectangle &rect) const;

		///<summary> Remember x,y should be the centre of the rectangle. </summary>
		/// <param name="x"> Centre X Component. </param>
		/// <param name="y"> Centre Y Component. </param>
		inline void set_position(const float x, const float y) { x_ = x; y_ = y; }

		/// <summary>
		/// Updates the rectangles position based on the FloatRect, Uses Rectangles Half_width/Height.
		/// Should ensure the sprite has not scaled since initialised.
		/// </summary>
		/// <param name="float_rect"> 
		/// Sfml rect representing the left, and top coordinate. 
		/// Best to use the FloatRect from Sprite getGlobalBounds. 
		/// As ensures all transforms,scales etc. are in effect. 
		/// </param>
		inline void set_position(sf::FloatRect float_rect) {
			x_ = float_rect.left + half_width_;
			y_ = float_rect.top + half_height_;
		}
		inline void set_half_width(const float half_width) { half_width_ = half_width; }
		inline void set_half_height(const float half_height) { half_height_ = half_height; }

		inline const float x() const { return x_; };
		inline const float y() const { return y_; };
		inline const float half_width() const { return half_width_; };
		inline const float half_height() const { return half_height_; };

	protected:
		float x_, y_, half_width_, half_height_;
	};
}

#endif