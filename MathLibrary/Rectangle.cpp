#include "rectangle.h"

#include "MathUtils.h"

namespace LittleLot {

	Rectangle::Rectangle() {
		x_ = 0;
		y_ = 0;
		half_width_ = 0;
		half_height_ = 0;
	}

	Rectangle::Rectangle(sf::FloatRect float_rect) {
		half_width_ = float_rect.width / 2.0f;
		half_height_ = float_rect.height / 2.0f;

		x_ = float_rect.left + half_width_;
		y_ = float_rect.top + half_height_;
	}

	Rectangle::Rectangle(float x, float y, float half_width, float half_height) {
		x_ = x;
		y_ = y;
		half_width_ = half_width;
		half_height_ = half_height;
	}

	const bool Rectangle::IntersectsWith(const Rectangle &rect) const {
		if ((x_ + half_width_) < (rect.x_ - rect.half_width_)) return false;
		if ((x_ - half_width_) > (rect.x_ + rect.half_width_)) return false;
		if ((y_ + half_height_) < (rect.y_ - rect.half_height_)) return false;
		if ((y_ - half_height_) > (rect.y_ + rect.half_height_)) return false;
		return true;
	}

	bool Rectangle::ContainsPoint(const sf::Vector2f &point) {
		if ((x_ + half_width_) < (point.x)) return false;
		if ((x_ - half_width_) > (point.x)) return false;
		if ((y_ + half_height_) < (point.y)) return false;
		if ((y_ - half_height_) > (point.y)) return false;
		return true;
	}

	bool Rectangle::WithinRect(const Rectangle &rect) {
		if ((x_ - half_width_) < (rect.x_ - rect.half_width_)) return false;
		if ((x_ + half_width_) > (rect.x_ + rect.half_width_)) return false;
		if ((y_ - half_height_) < (rect.y_ - rect.half_height_)) return false;
		if ((y_ + half_height_) > (rect.y_ + rect.half_height_)) return false;
		return true;
	}

	sf::Vector2f Rectangle::WithinRectOverlap(const Rectangle &rect) {
		// determine the overlap as there has been a collision
		sf::Vector2f overlap = sf::Vector2f(0, 0);
		// collision on top edge
		if ((y_ - half_height_) < (rect.y() - rect.half_height())) {
			float this_top = ((y_ - half_height_));
			float rect_top = (rect.y() - rect.half_height());
			// measure distance to top from object centre - height
			float amount_overlaped = rect_top - this_top;
			// move bot back to point of collision
			overlap.y += amount_overlaped;
		}
		// collision bottom edge
		if ((y_ + half_height_) > (rect.y() + rect.half_height())) {
			float this_bottom = (y_ + half_height_);
			float rect_bottom = (rect.y() + rect.half_height());
			// measure distance to bottom from object centre + height
			float amount_overlaped = this_bottom - rect_bottom;
			// move bot back to point of collision
			overlap.y -= amount_overlaped;
		}
		// collision on left edge
		if ((x_ - half_width_) < (rect.x() - rect.half_width())) {
			float this_left = (x_ - half_width_);
			float rect_left = (rect.x() - rect.half_width());
			// measure distance to top from object centre - height
			float amount_overlaped = rect_left - this_left;
			// move bot back to point of collision
			overlap.x += amount_overlaped;
		}
		// collision on right edge
		if ((x_ + half_width_) > (rect.x() + rect.half_width())) {
			float this_right = (x_ + half_width_);
			float rect_right = (rect.x() + rect.half_width());
			// measure distance to top from object centre - height
			float amount_overlaped = this_right - rect_right;
			// move bot back to point of collision
			overlap.x -= amount_overlaped;
		}
		return overlap;
	}

	const bool Rectangle::IntersectsPoint(Hit &hit, const sf::Vector2f point) const {
		float dx = point.x - x_;
		float px = half_width_ - abs(dx);
		if (px <= 0) {
			return false;
		}
		float dy = point.y - y_;
		float py = half_height_ - abs(dy);
		if (py <= 0) {
			return false;
		}
		if (px < py) {
			float sx = Sign(dx);
			hit.delta.x = px * sx;
			hit.normal.x = sx;
			hit.position.x = x_ + (half_width_*sx);
			hit.position.y = point.y;
		}
		else {
			float sy = Sign(dy);
			hit.delta.y = py * sy;
			hit.normal.y = sy;
			hit.position.x = point.x;
			hit.position.y = y_ + (half_height_*sy);
		}
		return true;
	}

	const bool Rectangle::IntersectsRectangle(Hit &hit, const Rectangle &rect) const {
		// check x bounds
		float dx = rect.x() - x_;
		float px = (rect.half_width() + half_width_) - abs(dx);
		if (px <= 0) {
			return false;
		}
		// check y bounds
		float dy = rect.y() - y_;
		float py = (rect.half_height() + half_height_) - abs(dy);
		if (py <= 0) {
			return false;
		}
		// determine axis of first overlap

		if (px < py) {
			float sx = Sign(dx);
			hit.delta.x = px*sx;
			hit.normal.x = sx;
			hit.position.x = x_ + (half_width_*sx);
			hit.position.y = rect.y();
		}
		else {
			float sy = Sign(dy);
			hit.delta.y = py*sy;
			hit.normal.y = sy;
			hit.position.x = rect.x();
			hit.position.y = y_ + (half_height_*sy);
		}
		return true;
	}

}