#ifndef _VECTOR_MANIPULATION_H
#define _VECTOR_MANIPULATION_H

#include <SFML/Graphics.hpp>

#include <cmath>

#include "MathUtils.h"

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	/// <summary> The non-square rooted length of the given vector. Saves a std::srtf caculation. </summary>
	/// <param name = 'vector'> The Vector to calculate the Length of. </param>
	/// <returns> Length of provided vector. </returns>	
	template<typename T>
	float LengthSqr(const sf::Vector2<T>& vector) {
		return vector.x*vector.x + vector.y*vector.y;
	}

	/// <summary> The length of the given vector. </summary>
	/// <param name = 'vector'> The Vector to calculate the Length of. </param>
	/// <returns> Length of provided vector. </returns>	
	template<typename T>
	float Length(const sf::Vector2<T>& vector) {
		return std::sqrtf(LengthSqr(vector));
	}

	/// <summary> Normalizes the given vector to a unit length vector. </summary>
	/// <param name = 'vector'> The Vector to normalize. </param>		
	template<typename T>
	void Normalize(sf::Vector2<T>& vector) {
		float length = Length(vector);
		vector.x = vector.x / length;
		vector.y = vector.y / length;
	}

	/// <summary> Caculates the dot product between the 2 given vectors. </summary>
	/// <param name = 'left_vector'> The left hand Vector for dot product. </param>
	/// <param name = 'right_vector'> The right hand Vector for dot product. </param>
	/// <returns> The dot product between the 2 vectors. </returns>	
	template<typename T>
	float DotProduct(const sf::Vector2<T>& left_vector, const sf::Vector2<T>& right_vector) {
		return left_vector.x*right_vector.x + left_vector.y*right_vector.y;
	}

	/// <summary> Lerps between 2 vectors, i.e.Smooth movement, independent lerping on x,y. </summary>
	/// <param name = 'start'> The start Vector to move from. </param>
	/// <param name = 'end'> The end Vector to move too. </param>
	/// <param name = 'time'> between 0 and 1 </param>
	/// <returns> Value generated between the two values at time value. </returns>	
	template<typename T>
	sf::Vector2<T> Lerp(const sf::Vector2<T>& start, const sf::Vector2<T>& end, const float time) {
		sf::Vector2<T> result;
		result.x = LittleLot::Lerp(start.x, end.x, time);
		result.y = LittleLot::Lerp(start.y, end.y, time);
		return result;
	}
}
#endif
