#ifndef _LEVEL_MAP_H
#define _LEVEL_MAP_H
#include <SFML/Graphics.hpp>

#include <vector>
#include "..\MathLibrary\Rectangle.h"
#include "Player.h"

/// <summary>
/// Generated Level map with collision and tile_sheet based texturing.
/// Loads and generates a level based on local map data. (simple fix, could always be imported from csv or something.)
/// </summary>
class LevelMap {
public:
	/// <summary>
	/// Default constructor, ensures Renderwindow refrence bound correctly.
	/// </summary>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	LevelMap(sf::RenderWindow& window);
	~LevelMap();

	/// <summary>
	/// Generates all of the collision, and textured tiles for the level.
	/// </summary>
	void Initialise();

	/// <summary>
	/// Renders the level
	/// </summary>
	void Render();
	
	/// <summary>
	/// Checks the given player against all rectangles for a collision and corrects the player to touching.
	/// </summary>
	/// <param name="player"> The player to check for collision against. </param>
	/// <returns> True if it has collided. </returns>
	bool CollisionCorrection(Player& player);

	int PixelWidth();
	int PixelHeight();
private:
	bool IsCollidable(int i);
	sf::RenderWindow& window_;
	sf::Sprite sprite_;
	std::vector<LittleLot::Rectangle> rects_;
	
	sf::Image tile_sheet_;
	sf::Image composite_background_;
	
	sf::Texture level_background_;
};
#endif // !_LEVEL_MAP_H



