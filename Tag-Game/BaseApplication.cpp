#include "BaseApplication.h"

BaseApplication::BaseApplication(sf::RenderWindow & window)
	: window_(window) {
	// Assign the window to the map, ensures we don't have to remember to in parent applications.
	level_map_ = new LevelMap(window);
}

BaseApplication::~BaseApplication() {
	// delete all players
	for each (Player* player in players_) {
		if (player) {
			delete player;
			player = nullptr;
		}
	}
	// clear out the vector
	players_.clear();

	// delete the map.
	if (level_map_) {
		delete level_map_;
		level_map_ = nullptr;
	}
}

void BaseApplication::Update(float delta_time) {
	for each (Player* player in players_) {
		player->Update(delta_time);
	}
}

void BaseApplication::Render() {
	level_map_->Render();
	for each (Player* player in players_) {
		player->Render();
	}
	
}
