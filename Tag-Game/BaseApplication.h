#ifndef _BASE_APPLICATION_H
#define _BASE_APPLICATION_H

#include <SFML/Graphics.hpp>

#include "..\NetworkProtocol\Messages.h"
#include "LevelMap.h"
#include "Player.h"

/// <summary>
/// Application interface, with basic functionally implemented.
/// 
/// Handles updating and rendering the players. 
/// Stores refrence to the current render window.
/// </summary>
class BaseApplication {
public:
	/// <summary>
	/// Default constructor, ensures Renderwindow refrence bound correctly.
	/// </summary>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	BaseApplication(sf::RenderWindow& window);
	virtual ~BaseApplication();
	
	/// <summary>
	/// Used to set up all the players based on network gamestate.
	/// Dependent on inherited class.
	/// </summary>
	virtual void Init() = 0;
	
	/// <summary>
	/// Interpolate and predict based on network_state to game state.
	/// Dependent on inherited class.
	/// </summary>
	/// <param name="frame"> Current frame of the app.</param>
	virtual void NetworkUpdate(sf::Uint64 frame) = 0;

	
	/// <summary>
	/// Update all players for this frame.
	/// </summary>
	/// <param name="delta_time"> Time in seconds since last update. </param>
	virtual void Update(float delta_time);

	/// <summary>
	/// Render all players for this frame.
	/// </summary>
	virtual void Render();

	/// <summary>
	/// Render The ImGui compmenets of the app.
	/// Remember to setup the window otherwise renders to default debug.
	/// Make sure to call this within the ImGui Render loop.
	/// </summary>
	/// <param name="open"> Allows the window to be closed with a X in top corner. 
	/// Determine to render based on the bool passed in. </param>
	virtual void RenderImGui(bool *open) = 0;
protected:
	sf::RenderWindow& window_;	
	sf::View view_;
	sf::Texture wallet_;
	sf::Texture money_;

	LevelMap* level_map_;
	std::vector<Player*> players_;

};
#endif // !_BASE_APPLICATION_H
