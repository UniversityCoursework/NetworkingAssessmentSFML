#ifndef _PLAYER_H
#define _PLAYER_H
#include <SFML/Graphics.hpp>

#include "..\NetworkProtocol\PlayerState.h"
#include "..\MathLibrary\Rectangle.h"

/// <summary>
/// Base Player class, others derived from this.
/// Has no user control, but works on available data, to move and render the player.
/// </summary>
class Player {
public:
	/// <summary>
	/// Default constructor, ensures Renderwindow refrence bound correctly.
	/// </summary>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	Player(sf::RenderWindow& window);
	virtual ~Player();
	/// <summary>
	/// Sets up the player according to the player state.
	/// </summary>
	/// <param name="player_state"> The network state of the player, used to position and set start variables. </param>
	/// <param name="texture"> The texture to apply to the sprite. </param>
	/// <param name="texture_rect"> SFML IntRect representing the size and position of the player on the texture. (useful if using spritesheets) </param>
	virtual void Initialise(PlayerState player_state, sf::Texture& texture, sf::IntRect texture_rect);	
	
	/// <summary>
	/// Update the position of the player for this frame.
	/// Returns true if there has been a change in player state (direction).
	/// </summary>
	/// <param name="delta_time"> Time in seconds since last update. </param>
	/// <returns> True if there has been a change in player state caused by input. </returns>
	virtual bool Update(float delta_time);

	/// <summary>
	/// Render the player sprite for this frame.
	/// </summary>
	virtual void Render();

	virtual sf::Vector2f Position();
	virtual sf::Vector2f Direction();

	LittleLot::Rectangle GetRect();
	PlayerType GetType();
	float GetMoveSpeed();

	virtual void SetPosition(sf::Vector2f new_pos);
	virtual void SetDirection(sf::Vector2f new_dir);

protected:
	sf::RenderWindow& window_;

	sf::Sprite sprite_;
	sf::Vector2f direction_;
	float move_speed_;

	LittleLot::Rectangle rect_;
	
	PlayerType type_;

};
#endif // !_PLAYER_H

