#ifndef _CONTROLLED_PLAYER_H
#define _CONTROLLED_PLAYER_H

#include "Player.h"

/// <summary>
/// A User Controleld Player class, derived from <seealso cref="Player"/>.
/// 
/// Controlled via mouse input by the user, allowing them to change direction.
/// </summary>
class ControlledPlayer : public Player {
public:
	/// <summary>
	/// Default constructor, ensures Renderwindow refrence bound correctly.
	/// </summary>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	ControlledPlayer(sf::RenderWindow& window);
	~ControlledPlayer();

	/// <summary>
	/// Update the position of the player for this frame.
	/// Handles user input to allow controlling of the player.
	/// Returns true if there has been a change in player state (direction).
	/// </summary>
	/// <param name="delta_time"> Time in seconds since last update. </param>
	/// <returns> True if there has been a change in player state caused by input. </returns>
	bool Update(float delta_time) override;
private:
	bool is_mouse_down_;
};
#endif // !_CONTROLLED_PLAYER_H

