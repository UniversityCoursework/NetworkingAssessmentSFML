#include "ControlledPlayer.h"

#include "..\MathLibrary\VectorManipulation.h"

#include "..\imgui\imgui.h"
#include "..\imgui\imgui-sfml.h"


ControlledPlayer::ControlledPlayer(sf::RenderWindow& window)
	:Player(window) {
	is_mouse_down_ = false;
}

ControlledPlayer::~ControlledPlayer() {
}

bool ControlledPlayer::Update(float delta_time) {
	// whether the player state needs to be updated on the server. (direction change etc.)
	bool server_update = false;

	// check if the mouse cursor is in focus. 
	// Fix for dealing with multiple windows with the same input.
	// As could control other players from one window.
	sf::IntRect rect(window_.getViewport(window_.getView()));
	if(rect.contains(sf::Mouse::getPosition(window_))) {

		if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && !is_mouse_down_) {
			
			is_mouse_down_ = true;
			// Test against ImGui to stop input when ImGui Is Active.
			if(!ImGui::IsMouseHoveringAnyWindow()) {
				// Mouse position relative to window
				// Direction from centre of screen to mouse
				direction_.x = static_cast<sf::Vector2f>(sf::Mouse::getPosition(window_)).x - static_cast<float>(window_.getSize().x / 2);
				direction_.y = static_cast<sf::Vector2f>(sf::Mouse::getPosition(window_)).y - static_cast<float>(window_.getSize().y / 2);
				LittleLot::Normalize(direction_);
				server_update = true;
			}
		}
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		direction_ = sf::Vector2f(0, 0);
		server_update = true;
	}
	if(!sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		is_mouse_down_ = false;
	}

	// Update base class. (movement etc.)
	Player::Update(delta_time);
	return server_update;
}
