#include "LevelMap.h"
#include <assert.h>

// Some simple const's to make it easier to read.
// currently a 1600*1600 square.
const int tile_size = 16;
const int level_width = 10;
const int level_height = 10;
const float scale = 4.0f;
const float start_x = -200;
const float start_y = -200;

/// <summary>
/// Texture space coordinates of different tiles on the spritesheet.
/// used to make it slightly easier.
/// </summary>
static const sf::IntRect texture_rects[] = {
	sf::IntRect(102,0,tile_size,tile_size),			// kFloorTile0,
	sf::IntRect(119,0,tile_size,tile_size),			// kFloorTile1,
	sf::IntRect(136,0,tile_size,tile_size),			// kFloorTile2,
	sf::IntRect(153,0,tile_size,tile_size),			// kFloorTile3,
	sf::IntRect(153,204,tile_size,tile_size),		// kTopLeftCorner,
	sf::IntRect(170,204,tile_size,tile_size),		// kTopRightCorner,
	sf::IntRect(153,221,tile_size,tile_size),		// kBotLeftCorner,
	sf::IntRect(170,221,tile_size,tile_size),		// kBotRightCorner,
	sf::IntRect(221,204,tile_size,tile_size),		// kLeftEdge,
	sf::IntRect(238,204,tile_size,tile_size),		// kRightEdge,
	sf::IntRect(221,221,tile_size,tile_size),		// kTopEdge,
	sf::IntRect(238,221,tile_size,tile_size),		// kBottomEdge,
	sf::IntRect(255,221,tile_size,tile_size),		// kPillar,
	sf::IntRect(340,68,tile_size,tile_size),		// kCrate
};

/// <summary>
/// All the different types of tiles, available on the texture, makes it easier to read.
/// Maps to <seealso cref="texture_rects"/>.
/// </summary>
enum TileType {
	kFloorTile0,
	kFloorTile1,
	kFloorTile2,
	kFloorTile3,
	kTopLeftCorner,
	kTopRightCorner,
	kBotLeftCorner,
	kBotRightCorner,
	kLeftEdge,
	kRightEdge,
	kTopEdge,
	kBottomEdge,
	kPillar,
	kCrate
};

///<summary>
/// Representation of the level using TileTypes.
/// : Note it is layed out in [y][x] space, as easier to read. 
/// </summary>
static const int level_layout_[level_height][level_width]{
	{ kTopLeftCorner, kBottomEdge	, kBottomEdge	, kBottomEdge	, kBottomEdge	, kBottomEdge	, kBottomEdge	, kBottomEdge	, kBottomEdge	, kTopRightCorner},
	{ kRightEdge	, kFloorTile0	, kFloorTile2	, kFloorTile0	, kFloorTile1	, kFloorTile0	, kFloorTile0	, kFloorTile2	, kFloorTile1	, kLeftEdge },
	{ kRightEdge	, kFloorTile3	, kFloorTile0	, kFloorTile3	, kFloorTile0	, kFloorTile2	, kFloorTile3	, kFloorTile0	, kFloorTile0	, kLeftEdge },
	{ kRightEdge	, kFloorTile0	, kFloorTile3	, kFloorTile0	, kFloorTile2	, kFloorTile0	, kFloorTile2	, kFloorTile3	, kFloorTile3	, kLeftEdge },
	{ kRightEdge	, kFloorTile3	, kFloorTile0	, kFloorTile2	, kFloorTile3	, kFloorTile2	, kFloorTile0	, kFloorTile0	, kFloorTile0	, kLeftEdge },
	{ kRightEdge	, kFloorTile1	, kFloorTile3	, kFloorTile0	, kFloorTile3	, kFloorTile1	, kFloorTile2	, kFloorTile2	, kFloorTile3	, kLeftEdge },
	{ kRightEdge	, kFloorTile2	, kFloorTile1	, kFloorTile3	, kFloorTile2	, kFloorTile0	, kFloorTile1	, kFloorTile3	, kFloorTile0	, kLeftEdge },
	{ kRightEdge	, kFloorTile1	, kFloorTile2	, kFloorTile1	, kFloorTile1	, kFloorTile3	, kFloorTile2	, kFloorTile0	, kFloorTile3	, kLeftEdge },
	{ kRightEdge	, kFloorTile3	, kFloorTile1	, kFloorTile0	, kFloorTile2	, kFloorTile0	, kFloorTile0	, kFloorTile3	, kFloorTile0	, kLeftEdge },
	{ kBotLeftCorner, kTopEdge		, kTopEdge		, kTopEdge		, kTopEdge		, kTopEdge		, kTopEdge		, kTopEdge		, kTopEdge		, kBotRightCorner },
};



LevelMap::LevelMap(sf::RenderWindow& window)
	:window_(window) {
}

LevelMap::~LevelMap() {
}

void LevelMap::Initialise() {
	
	float pos_x = start_x;
	float pos_y = start_y;
	float real_size = tile_size*scale;
	float half_size = real_size / 2.0f;
	// Generate all of the collision rectangles.
	// Would be nice to merge side by side ones together, and reduce the number of rectangles ( also make edge collision smoother.
	for (int y = 0; y < level_height; y++) {
		for (int x = 0; x < level_width; x++) {
			int rect = level_layout_[y][x];
			if (IsCollidable(rect)) {
				rects_.push_back(LittleLot::Rectangle(pos_x + half_size, pos_y + half_size, half_size, half_size));
			}
			pos_x += real_size;
		}
		pos_x = start_x;
		pos_y += real_size;
	}



	// Fix for multiple sprites showing gaps between them or tears, so merge all the tiles into a big image.
	
	// load the tile sheet into an image file.
	tile_sheet_.loadFromFile("../res/tilesheet_transparent_tiles.png");

	// Create a 20x20 image filled with black color
	composite_background_.create(level_width*tile_size, level_height*tile_size, sf::Color::Black);

	// Copy the tiles from the tile sheet onto the background image.
	pos_x = 0;
	pos_y = 0;
	for (int y = 0; y < level_height; y++) {
		for (int x = 0; x < level_width; x++) {
			int rect = level_layout_[y][x];
			composite_background_.copy(tile_sheet_, static_cast<unsigned int>(pos_x), static_cast<unsigned int>(pos_y), texture_rects[rect]);
			pos_x += tile_size;
		}
		pos_x = 0;
		pos_y += tile_size;
	}
	// Store the background image, in the texture for easier rendering,
	level_background_.loadFromImage(composite_background_);
	// store it all in the sprite.
	sprite_.setPosition(start_x, start_y);
	sprite_.setTexture(level_background_);
	sprite_.setScale(scale, scale);
}

void LevelMap::Render() {
	window_.draw(sprite_);
}

bool LevelMap::CollisionCorrection(Player & player) {
	LittleLot::Hit hit;
	bool hit_something = false;
	for each (LittleLot::Rectangle rect in rects_) {
		if (player.GetRect().IntersectsRectangle(hit, rect)) {
			player.SetPosition(player.Position() - hit.delta);	
			hit_something = true;
		}
	}
	return hit_something;
}

int LevelMap::PixelWidth() {
	return level_width*tile_size*tile_size;
}

int LevelMap::PixelHeight() {
	return level_height*tile_size*tile_size;
}

bool LevelMap::IsCollidable(int i) {
	return (i == kTopLeftCorner ||
			i == kTopRightCorner ||
			i == kBotLeftCorner ||
			i == kBotRightCorner ||
			i == kLeftEdge ||
			i == kRightEdge ||
			i == kTopEdge ||
			i == kBottomEdge ||
			i == kPillar ||
			i == kCrate);
}
