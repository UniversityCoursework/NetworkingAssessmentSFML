#include "Player.h"
const float move_speed = 100.0f;
const float scale = 0.5f;

Player::Player(sf::RenderWindow& window)
	:window_(window) {
	direction_ = sf::Vector2f(0, 0);
}



void Player::Initialise(PlayerState player_state, sf::Texture & texture, sf::IntRect texture_rect) {

	sprite_.setPosition(player_state.position);
	sprite_.setTexture(texture);
	sprite_.setTextureRect(texture_rect);
	sprite_.setScale(scale, scale);
	// Set independent of scaling, but based on the texture, so use the passed in texture_rect,was mainly for rotations etc.
	sprite_.setOrigin(static_cast<float>(texture_rect.width / 2.0f), static_cast<float>(texture_rect.height / 2.0f));
	move_speed_ = move_speed;
	direction_ = player_state.direction;
	rect_ = LittleLot::Rectangle(sprite_.getGlobalBounds());
	type_ = player_state.player_type;
}

Player::~Player() {
}

bool Player::Update(float delta_time) {
	sprite_.move(direction_*move_speed_*delta_time);
	// Set the collision rectangle to match the srpite.
	rect_.set_position(sprite_.getGlobalBounds());

	// No input so just return false.
	return false;
}

void Player::Render() {
	window_.draw(sprite_);
}

sf::Vector2f Player::Position() {
	return sprite_.getPosition();
}

sf::Vector2f Player::Direction() {
	return direction_;
}

void Player::SetPosition(sf::Vector2f new_pos) {
	sprite_.setPosition(new_pos);
	// Set the collision rectangle to match the new sprite position.
	rect_.set_position(sprite_.getGlobalBounds());
}

void Player::SetDirection(sf::Vector2f new_dir) {
	direction_ = new_dir;
}

LittleLot::Rectangle Player::GetRect() {
	return rect_;
}

PlayerType Player::GetType() {
	return type_;
}

float Player::GetMoveSpeed() {
	return move_speed_;
}
