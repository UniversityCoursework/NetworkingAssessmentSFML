#ifndef _TIMER_H
#define _TIMER_H

/// <summary>
/// Simple inpercise timer wrapper.
/// 
/// Useful for stuff were we just want a rough time, not incredably accurate.
/// </summary>
class Timer {
public:
	Timer();
	/// <summary>
	/// Set up the timer with countdown time, before it is finished.
	/// </summary>
	/// <param name="time">The amount of time to pass in milliseconds. </param>
	Timer(int time);
	
	/// <summary>
	///	Update the timer with the amount of time passed since last update.
	/// </summary>
	/// <param name="delta_time"> Time in milliseconds since last update.</param>
	void Update(int delta_time);

	
	/// <summary>
	/// Reset timer back to zero, but maintain countdown time.
	/// </summary>
	void Reset();

	
	/// <summary>
	/// Check if the timer has reached the countdown time.
	/// </summary>
	/// <returns> Returns true if it has finished. </returns>
	bool IsFinished();
	
	/// <summary>
	/// Change time at which the timer will finish.
	/// </summary>
	/// <param name="time"> The amount of time to pass in milliseconds. </param>
	void SetTimer(int time);

private:
	bool is_finished_;
	int timer_;
	int time_;
};
#endif
