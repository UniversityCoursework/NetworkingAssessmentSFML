#ifndef _PLAYER_STATE_H
#define _PLAYER_STATE_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <vector>

/// <summary>
/// The type of player, in relation to texture, and collision needed.
/// </summary>
enum PlayerType {
	kWallet,
	kMoney,
	kSpectator,
	kDead
};

/// <summary>
/// Simple Utility function to ouput Enum in ImGui.
/// </summary>
/// <param name="type"> Type to be converted into a string. </param>
/// <returns> Player Type formated as a string. Useable for ImGui etc.</returns>
static std::string PlayerTypeToString(PlayerType type) {
	switch (type) {
		case kWallet:
			return "Wallet";
			break;
		case kMoney:
			return "Money";
			break;
		case kSpectator:
			return "Spectator";
			break;
		case kDead:
			return "Dead";
			break;
		default:
			return "Corrupt Data";
			break;
	}
}

/// <summary>
/// Network Player State.
/// Used to store the useful network information of the player.
/// </summary>
struct PlayerState {
	sf::Uint8 player_id = 0;
	std::string username = "Idiot";
	sf::Vector2f position;
	sf::Vector2f direction;
	PlayerType player_type = kSpectator;
	sf::Uint64 last_frame=0;

	friend sf::Packet& operator <<(sf::Packet& packet, const PlayerState& player_state) {
		return packet
			<< player_state.player_id
			<< player_state.username
			<< player_state.position.x
			<< player_state.position.y
			<< player_state.direction.x
			<< player_state.direction.y
			<< static_cast<sf::Uint8>(player_state.player_type)	// Ensure enum is cast properly for storage in packet.
			<< player_state.last_frame;
	}
	friend sf::Packet& operator >> (sf::Packet& packet, PlayerState& player_state) {
		sf::Uint8 enum_player_type;
		packet
			>> player_state.player_id
			>> player_state.username
			>> player_state.position.x
			>> player_state.position.y
			>> player_state.direction.x
			>> player_state.direction.y
			>> enum_player_type
			>> player_state.last_frame;
		player_state.player_type = static_cast<PlayerType>(enum_player_type);	// Try cast packet info is back to PlayerType enum correctly.
		return packet;

	}
};

#endif // !_PLAYER_STATE_H