#ifndef CONNECTION_H
#define CONNECTION_H
#include <SFML/Network.hpp>


/// <summary>
/// Each instance of this class represents a connected client. 
/// </summary>
class Connection {
public:
	/// <summary>
	/// A connection must be created with provided information.
	/// Ensures it is valid, and useful.
	/// </summary>
	/// <param name="address"> Ipv4 address of the connection. </param>
	/// <param name="port"> The port on which the connection is on. </param>
	/// <param name="new_id"> The assigned ID for this connection (Generaly connected to a player state) </param>
	Connection(sf::IpAddress address, unsigned short port, sf::Uint8 new_id);

	~Connection();

	sf::IpAddress Address();
	unsigned short Port();
	sf::Uint8 ConnectionId();
		
	/// <summary>
	/// Overload of the Comparision of 2 connections.
	/// Returns true if the address, and port match.
	/// </summary>
	/// <param name="connection"> The Connection to compare against.</param>
	/// <returns> True if the address, and port are equal.</returns>
	const bool operator== (const Connection &connection);

	/// <summary>
	/// Overload Copy operator.
	/// 
	/// Ensures the connection address, and port are copied over, but doesn't transfer the ID.
	/// </summary>
	/// <param name="connection"> The connection to copy from. </param>
	/// <returns> Pointer to itself. </returns>
	Connection& operator= (const Connection connection);

private:
	sf::Uint8 connection_id;
	sf::IpAddress address_;
	unsigned short port_;

};
#endif