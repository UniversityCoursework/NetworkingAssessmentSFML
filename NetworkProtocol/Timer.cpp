#include "Timer.h"

Timer::Timer() {
}

Timer::Timer(int time) {
	time_ = time;
}

void Timer::Update(int delta_time) {
	// check if it has finished.
	if (timer_ > time_) {
		is_finished_ = true;
	}
	// otherwise incremnt the amount of time passed.
	else {
		timer_ += delta_time;
	}
}

void Timer::Reset() {
	// reset the timer, and set to unfinished.
	timer_ = 0;
	is_finished_ = false;
}

bool Timer::IsFinished() {
	return is_finished_;
}

void Timer::SetTimer(int time) {
	time_ = time;
}
