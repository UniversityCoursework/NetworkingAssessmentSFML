#include "Connection.h"

Connection::Connection(sf::IpAddress address, unsigned short port, sf::Uint8 new_id) {
	address_ = address;
	port_ = port;
	connection_id = new_id;
}

Connection::~Connection() {
}

sf::IpAddress Connection::Address() {
	return address_;
}

unsigned short Connection::Port() {
	return port_;
}

sf::Uint8 Connection::ConnectionId() {
	return connection_id;
}

const bool Connection::operator==(const Connection & connection) {
	return (address_ == connection.address_ && port_ == connection.port_);
}

Connection & Connection::operator=(const Connection connection) {
	address_ = connection.address_;
	port_ = connection.port_;
	return *this;
}

