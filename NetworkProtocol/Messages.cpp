#include "Messages.h"

/// <summary>
/// Excessive overload of << operators due to SFML packet implementation.
/// Ensures the information is packtaged in a Uniform manner.
/// </summary>

sf::Packet & operator<<(sf::Packet & packet, const MessageType & message) {
	// Store it as Uint8 so uniform storage, otherwise could be stored differently depenedent on compiler/OS.
	return packet << static_cast<sf::Uint8>(message);
}

sf::Packet & operator>>(sf::Packet & packet, MessageType & message) {
	sf::Uint8 enum_message_type;
	packet >> enum_message_type;
	// Make sure it is converted back into an enum.
	message = static_cast<MessageType>(enum_message_type);
	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const ConnectionRequest & message) {
	return packet << message.username;
}

sf::Packet & operator>>(sf::Packet & packet, ConnectionRequest & message) {
	return packet >> message.username;
}

sf::Packet & operator<<(sf::Packet & packet, const ConnectionResponse & message) {
	return packet
		<< message.is_connected
		<< message.new_player_state
		<< message.server_frame;
}

sf::Packet & operator>>(sf::Packet & packet, ConnectionResponse & message) {	
	return packet
		>> message.is_connected
		>> message.new_player_state
		>> message.server_frame;	 
}

sf::Packet & operator<<(sf::Packet & packet, const PlayerUpdate & message) {
	return packet << message.new_player_state;
}

sf::Packet & operator>>(sf::Packet & packet, PlayerUpdate & message) {
	return packet >> message.new_player_state;
}

sf::Packet & operator<<(sf::Packet & packet, const PlayerCollision & message) {
	return packet << message.sending_player
		<< message.collided_with_id
		<< message.current_frame;
}

sf::Packet & operator>>(sf::Packet & packet, PlayerCollision & message) {
	return packet >> message.sending_player
		>> message.collided_with_id
		>> message.current_frame;
}

sf::Packet & operator<<(sf::Packet & packet, const CollisionResponse & message) {
	return packet << message.response;
}

sf::Packet & operator>>(sf::Packet & packet, CollisionResponse & message) {
	return packet >> message.response;
}


sf::Packet & operator<<(sf::Packet & packet, const GameState & message) {
	// Specfied specifc size, as may not wish to remove all  playerstate data.
	packet << message.array_size
		<< message.game_running
		<< message.server_frame;
	// Package each player seperatly, so stored correctly in Packet.
	for each (PlayerState player in message.player_states) {
		packet << player;
	}
	return packet;
}

sf::Packet & operator>>(sf::Packet & packet, GameState & message) {
	packet >> message.array_size
		>> message.game_running
		>> message.server_frame;
	// Run through the packet removing each player indivually and adding to the GameState.
	for (int i = 0; i < message.array_size; i++) {
		PlayerState new_player;
		packet >> new_player;
		message.player_states.push_back(new_player);
	}
	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const GameEnd & message) {
	return packet << message.winner_id;
}

sf::Packet & operator>>(sf::Packet & packet, GameEnd & message) {
	return packet >> message.winner_id;
}
