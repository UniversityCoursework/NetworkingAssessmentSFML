#ifndef _MESSAGES_H
#define _MESSAGES_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <vector>

#include "PlayerState.h"

/// <summary>
/// The Type of message that has been sent.
/// This will be taken out of the packet first to determine the message sent.
/// and how much information can be extracted.
/// </summary>
enum MessageType {
	kConnectionRequest = 1,
	kConnectionResponse = 2,
	kPlayerUpdate = 3,
	kPlayerCollision = 4,
	kCollisionResponse = 5,
	kGameState = 6,
	kGameEnd = 7
};
sf::Packet& operator<<(sf::Packet& packet, const MessageType& message);
sf::Packet& operator>>(sf::Packet& packet, MessageType& message);

/// <summary>
/// User Requesting a connection to the game, contain only the username.
/// Ip, and Port taken from recieve data.
/// </summary>
struct ConnectionRequest {
	std::string username = "Idiot";

	friend sf::Packet& operator<<(sf::Packet& packet, const ConnectionRequest& message);
	friend sf::Packet& operator>>(sf::Packet& packet, ConnectionRequest& message);
};

// 

/// <summary>
/// Whether the user has connected succesfully.
/// Telling the user if they have connected.
/// If they have their new client id, and player type. As well as the current servers frame count. 
/// </summary>
struct ConnectionResponse {
	bool is_connected = false;
	PlayerState new_player_state;
	sf::Uint64 server_frame = 0;
	friend sf::Packet& operator<<(sf::Packet& packet, const ConnectionResponse& message);
	friend sf::Packet& operator>>(sf::Packet& packet, ConnectionResponse& message);
};


/// <summary>
/// Used to tell the server the client has changed direction, and where it changed direction.
/// Or if it has changed type (spectator/coin/wallet etc.)
/// </summary>
struct PlayerUpdate {
	PlayerState new_player_state;

	friend sf::Packet& operator<<(sf::Packet& packet, const PlayerUpdate& message);
	friend sf::Packet& operator>>(sf::Packet& packet, PlayerUpdate& message);
};


/// <summary>
/// Tells the server the client thinks a collision should happen between
/// these 2 players. Server should check if it has already happened,
/// if not double check it with player data. 
/// </summary>
struct PlayerCollision {
	PlayerState sending_player;
	sf::Uint8 collided_with_id = 0;
	sf::Uint64 current_frame = 0;

	friend sf::Packet& operator<<(sf::Packet& packet, const PlayerCollision& message);
	friend sf::Packet& operator>>(sf::Packet& packet, PlayerCollision& message);
};


/// <summary>
/// Servers decision on the collision sent to the requesting player.
/// A gamestate update should also be broadcast to all players.
/// </summary>
struct CollisionResponse {
	bool response = false;

	friend sf::Packet& operator<<(sf::Packet& packet, const CollisionResponse& message);
	friend sf::Packet& operator>>(sf::Packet& packet, CollisionResponse& message);
};


/// <summary>
/// General update of all player's states in the game.
/// </summary>
struct GameState {
	sf::Uint8 array_size = 0;
	std::vector<PlayerState> player_states;
	bool game_running = false;
	sf::Uint64 server_frame = 0;
	friend sf::Packet& operator<<(sf::Packet& packet, const GameState& message);
	friend sf::Packet& operator>>(sf::Packet& packet, GameState& message);

};


/// <summary>
/// The Game has ended notify all players the winner's id.
/// </summary>
struct GameEnd {
	sf::Uint8 winner_id = 0;

	friend sf::Packet& operator<<(sf::Packet& packet, const GameEnd& message);
	friend sf::Packet& operator>>(sf::Packet& packet, GameEnd& message);
};
#endif
