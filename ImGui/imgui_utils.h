#pragma once

namespace ImGui {
	/// <summary>
	/// Almost Redundant wrap up of rendering FPS to an ImGui window.
	/// Rember to set up a window first or will render to default debug.
	/// </summary>
	void RenderFps();
}