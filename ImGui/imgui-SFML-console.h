#ifndef _SFML_CONSOLE_H
#define _SFML_CONSOLE_H

#include <string>

namespace ImGui {
	namespace SFML {

		/// <summary>
		/// Simplified Console for ImGui.
		/// <para>
		/// Renders to the console window attached to the bottom section of the screen.
		/// Console fills from the top, send older messages offscreen downwards.
		/// </para>
		/// <seealso cref="GetConsole()"/>
		/// </summary>		
		class Console {
		public:
			Console();
			~Console();
			/// <summary>
			/// Adds a new line to the top of the console.
			/// </summary>
			/// <param name="text"></param>
			void AddLine(std::string text);

			/// <summary>
			/// Renders the ImGui Console window.
			/// Renders it at the bottom of the screen, at full width.
			/// </summary>
			/// <param name="open"> Allows the window to be closed with a X in top corner. 
			/// Determine to render based on the bool passed in. </param>
			void RenderImGui(bool *open);
		private:
			/// <summary>
			/// Entire current console log.
			/// TODO: Ensure doesn't get to big.
			/// </summary>
			std::string console_text_;
			/// <summary>
			/// Tracked number of console line, Appended to the begning of every Line.
			/// Used to differentiate between similiar lines. (i.e. adding the sameline multiple times.)
			/// </summary>
			uint64_t line_number_;
		};

		/// <summary>
		/// Returns Global console.
		/// Ensures the application only uses one console and is allways output to the same one.
		/// </summary>
		/// <returns></returns>
		Console& GetConsole();
		
	}
}

#endif