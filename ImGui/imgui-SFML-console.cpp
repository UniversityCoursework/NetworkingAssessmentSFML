#include "imgui-SFML-console.h"

#include "..\imgui\imgui.h"

ImGui::SFML::Console imgui_console_;

ImGui::SFML::Console::Console() {
	console_text_ = "";
	line_number_ = 0;

}

ImGui::SFML::Console::~Console() {
}

void ImGui::SFML::Console::AddLine(std::string text) {	
	console_text_ = std::to_string(line_number_) + ":"+text + "\n" + console_text_;
	line_number_++;

}

void ImGui::SFML::Console::RenderImGui(bool *open) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(io.DisplaySize.x, 100);
	ImVec2 window_pos = ImVec2(
		0,
		(io.DisplaySize.y) - (window_size.y));

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);

	if (!ImGui::Begin("Console Output", open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}
	ImGui::TextWrapped(console_text_.c_str());
	ImGui::End();

}

ImGui::SFML::Console & ImGui::SFML::GetConsole() {
	return imgui_console_;
}
