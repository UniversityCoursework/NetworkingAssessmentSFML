#include "..\ImGui\imgui_utils.h"
#include "..\imgui\imgui.h"

void ImGui::RenderFps() {
	ImGui::Text("FPS: %.1f, %.3f ms", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
}
