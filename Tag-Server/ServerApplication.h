#ifndef _SERVER_APPLICATION_H

#define _SERVER_APPLICATION_H

#include "..\Tag-Game\BaseApplication.h"
#include "NetworkServer.h"

/// <summary>
/// Handles displaying a server representation of the current server Game State. Derived from <seealso cref="BaseApplication"/>
/// Display last know positions. 
/// Whilst exposing the follow camera, to follow sepecific players.
/// </summary>
class ServerApplication :public BaseApplication {
public:
	/// <summary>
	/// Ensures Renderwindow refrence bound correctly, and refrence to network server is bound correctly.
	/// </summary>
	/// <param name="server"> Server to show GameState information about.</param>
	/// <param name="window"> The window everything is being rendered to in sfml. </param>
	ServerApplication(NetworkServer& server, sf::RenderWindow& window);
	~ServerApplication();

	/// <summary>
	/// Sets up all the players based on network gamestate.
	/// Sets up the level map as well.
	/// </summary>
	void Init() override;

	/// <summary>	
	/// Sets players positions, and directions directly to last know position and direction in server game_state.	
	/// </summary>
	/// <param name="frame"> Current frame of the app.</param>
	void NetworkUpdate(sf::Uint64 frame) override;

	/// <summary>
	/// Update all players for basic collision, Centres the view on the selecte player in the UI.
	/// </summary>
	/// <param name="delta_time"> Time in seconds since last update. </param>
	void Update(float delta_time) override;

	/// <summary>
	/// Displays the selected players username and ID.
	/// Exposes controls to change selected player.
	/// </summary>
	/// <param name="open"> Allows the window to be closed with a X in top corner. 
	/// Determine to render based on the bool passed in. </param>
	void RenderImGui(bool *open) override;
private:
	int selected_player_;
	NetworkServer& server_;

	GameState& game_state_;
};

#endif // !_SERVER_APPLICATION_H

