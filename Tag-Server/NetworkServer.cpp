#include "NetworkServer.h"
#include "..\imgui\imgui.h"
const unsigned short server_port = 50002;

typedef std::vector<Connection>::iterator ConnIter;
NetworkServer::NetworkServer() :
	console(ImGui::SFML::GetConsole()) {
	socket_.bind(server_port);
	socket_.setBlocking(false);
	server_tick_.SetTimer(30);

	// Set Up PlayerStates, add as many players as connected clients we want.
	// player_id's cannot match.
	PlayerState new_player;
	new_player.player_id = 0;
	new_player.position = sf::Vector2f(200, 200);
	new_player.player_type = PlayerType::kWallet;
	current_game_state_.player_states.push_back(new_player);

	new_player.player_id = 1;
	new_player.position = sf::Vector2f(100, 100);
	new_player.player_type = PlayerType::kMoney;
	current_game_state_.player_states.push_back(new_player);

	new_player.player_id = 2;
	new_player.position = sf::Vector2f(0, 100);
	new_player.player_type = PlayerType::kMoney;
	current_game_state_.player_states.push_back(new_player);

	new_player.player_id = 3;
	new_player.position = sf::Vector2f(100, 0);
	new_player.player_type = PlayerType::kMoney;
	current_game_state_.player_states.push_back(new_player);
}

NetworkServer::~NetworkServer() {
}

void NetworkServer::Update(sf::Time delta_time) {
	frame_ += delta_time.asMilliseconds();
	server_tick_.Update(delta_time.asMilliseconds());
	ProcessRecievedPackets();
	
	// Server GameStateUpdate;
	if (server_tick_.IsFinished()) {
		server_tick_.Reset();
		BroadcastGameStateUpdate();
	}
}

void NetworkServer::RenderImGui(bool *open) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(300, 400);
	ImVec2 window_pos = ImVec2(
		0,
		20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if (!ImGui::Begin("Server Debug", open, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}

	ImGui::Text("GameState - Connected %d : %d", clients_.size(), current_game_state_.player_states.size());
	// Drop down menus for all connected players, shows there current network state.
	for (size_t i = 0; i < clients_.size(); i++) {
		PlayerState player = current_game_state_.player_states[i];
		if (ImGui::TreeNode((void*)(intptr_t)player.player_id, "ID %d, %s", player.player_id, player.username.c_str())) {
			ImGui::Text("Pos: X: %.3f,Y: %.3f", player.position.x, player.position.y);
			ImGui::Text("Dir: X: %.3f,Y: %.3f", player.direction.x, player.direction.y);
			ImGui::Text("Type: %s", PlayerTypeToString(player.player_type).c_str());
			ImGui::TreePop();
		}
	}
	ImGui::Text("Game Running: %s", current_game_state_.game_running ? "true" : "false");
	ImGui::Text("ServerFrame: %d", current_game_state_.server_frame);
	ImGui::End();
}

bool NetworkServer::IsGameRunning() {
	return current_game_state_.game_running;
}

GameState & NetworkServer::GetNetworkState() {
	return current_game_state_;
}

sf::Uint64 NetworkServer::Frame() {
	return frame_;
}

void NetworkServer::ProcessRecievedPackets() {
	sf::Packet recieved_packet;
	sf::IpAddress sender;
	unsigned short sender_port;
	MessageType message_type;
	if (socket_.receive(recieved_packet, sender, sender_port) != sf::Socket::Done) {
		return;
	}
	recieved_packet >> message_type;
	switch (message_type) {
		case kConnectionRequest:
		{
			console.AddLine("Connection Request");
			Connection new_client = Connection(sender, sender_port, 0);
			ConnIter client_to_find = std::find(clients_.begin(), clients_.end(), new_client);

			sf::Packet reply_packet;
			reply_packet << MessageType::kConnectionResponse;
			ConnectionResponse conn_resp;
			conn_resp.server_frame = frame_;

			// Check if the Client doesnt exist.
			if (client_to_find == clients_.end()) {
				// Check if space for more clients.
				if (clients_.size() < current_game_state_.player_states.size()) {
					// New user so add them
					sf::Uint8 new_id = current_game_state_.player_states[clients_.size()].player_id;
					clients_.push_back(Connection(sender, sender_port, new_id));
					ConnectionRequest conn_req;
					recieved_packet >> conn_req;
					current_game_state_.player_states[new_id].username = conn_req.username;

					// Reply with them added.
					conn_resp.is_connected = true;
					conn_resp.new_player_state = current_game_state_.player_states[new_id];

					reply_packet << conn_resp;
					socket_.send(reply_packet, sender, sender_port);

					// If game is now full the game should start running.
					if (clients_.size() >= current_game_state_.player_states.size()) {
						current_game_state_.game_running = true;
					}
				}
				else {
					// no space so send denial					
					conn_resp.is_connected = false;
					reply_packet << conn_resp;
					socket_.send(reply_packet, sender, sender_port);
				}
			}
			else {
				// User already exists, so just resend connection response.
				// Incase of packet drop.
				conn_resp.is_connected = true;
				conn_resp.new_player_state = current_game_state_.player_states[client_to_find->ConnectionId()];
				reply_packet << conn_resp;
				socket_.send(reply_packet, client_to_find->Address(), client_to_find->Port());
			}
			break;
		}
		case kPlayerUpdate:
		{
			PlayerUpdate new_update;
			recieved_packet >> new_update;
			console.AddLine("Player Updated " + std::to_string(new_update.new_player_state.player_id));
			
			// If it is a new update update the server.
			if (new_update.new_player_state.last_frame > current_game_state_.player_states[new_update.new_player_state.player_id].last_frame) {
				current_game_state_.player_states[new_update.new_player_state.player_id] = new_update.new_player_state;
				console.AddLine("Player Updated Success");
			}
			break;
		}

		case kPlayerCollision:
		{
			console.AddLine("Player Collision Requested");
			sf::Packet reply_packet;
			reply_packet << MessageType::kCollisionResponse;
			// TODO: Collision Response, Detect/run_test, Decide if possible.
			// Update server game state, and send out to all clients.
			// Check for game end, send out to all clients, and keep sending until get ack from all clients.

			break;
		}
		default:
			console.AddLine("Packet Corrupt");
			break;
	}



}

void NetworkServer::BroadcastGameStateUpdate() {
	sf::Packet reply_packet;
	current_game_state_.array_size = static_cast<sf::Uint8>(clients_.size());
	current_game_state_.server_frame = frame_;
	reply_packet << static_cast<sf::Uint8>(MessageType::kGameState);
	reply_packet << current_game_state_;
	for each (Connection client in clients_) {
		socket_.send(reply_packet, client.Address(), client.Port());
	}
}
