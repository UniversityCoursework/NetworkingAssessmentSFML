#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

#include "..\imgui\imgui.h"
#include "..\imgui\imgui-sfml.h"
#include "..\imgui\imgui_style.h"
#include "..\ImGui\imgui-SFML-console.h"
#include "..\ImGui\imgui_utils.h"

#include "NetworkServer.h"
#include "ServerApplication.h"

#ifdef _MSC_VER
#    pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

void main() {
	// Create the Render window.
	sf::RenderWindow window(sf::VideoMode(800, 600), "Server", sf::Style::Default);
	window.setVerticalSyncEnabled(true);
	window.setActive();
	// Setup ImGui.
	ImGui::SFML::Init(window);
	ImGui::SetupImGuiStyle(true, 0.8f);
	// Create a clock for measuring the time elapsed.
	sf::Clock clock;
	// Store in sf::Time so can decide which time format.
	sf::Time delta_time;
	// Background Colour, on clear.
	sf::Color background_colour_ = sf::Color(0x5C1471FF);
	// Create a shared output console via ImGui, lets anything output info to the console.
	ImGui::SFML::Console& console = ImGui::SFML::GetConsole();

	NetworkServer network_server;
	ServerApplication server_application(network_server,window);
	bool show_server_debug = true;
	bool show_console_output = true;
	bool show_app_info = true;
	bool init_game = false;
	
	// As game starts with only rendering imgui, not sfml stuff.
	window.resetGLStates();

	// Start the game loop
	while (window.isOpen()) {
		// Clear Screen.
		window.clear(background_colour_);
		// Update with time passed since last frame.
		delta_time = clock.restart();
		// Process events, as long as their is events to process.
		sf::Event event;
		while (window.pollEvent(event)) {
			ImGui::SFML::ProcessEvent(event);
			// Close window: exit
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			// Escape key: exit
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape)) {
				window.close();
			}
		}

		// Logic.
		network_server.Update(delta_time);
		if (network_server.IsGameRunning()) {
			if (!init_game) {
				server_application.Init();
				init_game = true;
				
			}
			server_application.NetworkUpdate(network_server.Frame());
			server_application.Update(delta_time.asSeconds());
		}

		// Render.
		if (init_game) {
			server_application.Render();
		}
		
		// Allows ImGui to change OpenGl stuff.
		// Without having to reset everything or worry about breaking sfml.
		window.pushGLStates();
		ImGui::SFML::Update(window, delta_time);		

		if (ImGui::BeginMainMenuBar()) {
			// Push it to screen edge.
			ImGui::SameLine(static_cast<float>(window.getSize().x) - 200.f);
			if (ImGui::BeginMenu("Debug")) {
				ImGui::MenuItem("Show Console", NULL, &show_console_output);
				ImGui::MenuItem("Show Server", NULL, &show_server_debug);

				if (init_game) {
					ImGui::MenuItem("Show App", NULL, &show_app_info);
				}

				ImGui::EndMenu();
			}
			ImGui::RenderFps();
			ImGui::EndMainMenuBar();
		}

		if (show_console_output) {
			console.RenderImGui(&show_console_output);
		}
		if (show_server_debug) {
			network_server.RenderImGui(&show_server_debug);
		}
		if (show_app_info) {
			if (init_game) {
				server_application.RenderImGui(&show_app_info);
			}
		}

		ImGui::Render();
		window.popGLStates();
		//*/
		// Finally, display the rendered frame on screen
		window.display();
	}
	ImGui::SFML::Shutdown();
}
