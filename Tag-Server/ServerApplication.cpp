#include "ServerApplication.h"

#include "..\ImGui\imgui.h"
#include "..\ImGui\imgui-SFML.h"

static const sf::IntRect texture_rects[] = {
	sf::IntRect(0,0,152,168),	// wallet
	sf::IntRect(0,0,200,120),	// money
};

ServerApplication::ServerApplication(NetworkServer& server, sf::RenderWindow & window)
	:BaseApplication(window),server_(server),game_state_(server.GetNetworkState()) {


	assert(wallet_.loadFromFile("../res/Wallet.png"));
	assert(money_.loadFromFile("../res/Money.png"));

	selected_player_ = 0;
}

ServerApplication::~ServerApplication() {
	BaseApplication::~BaseApplication();
}

void ServerApplication::Init() {
	for each (PlayerState new_state in game_state_.player_states) {
		Player* new_player = new Player(window_);
		switch (new_state.player_type) {
			case PlayerType::kWallet:
			{
				new_player->Initialise(new_state, wallet_, texture_rects[0]);
				break;
			}
			case PlayerType::kMoney:
			{
				new_player->Initialise(new_state, money_, texture_rects[1]);
				break;
			}
			default:
				break;
		}
		players_.push_back(new_player);
	}

	level_map_->Initialise();

}

void ServerApplication::NetworkUpdate(sf::Uint64 frame) {
	// TODO: Change to prediction and interpolation, not direct set to gamestate.
	int i = 0;
	for each (Player* player in players_) {
		player->SetPosition(game_state_.player_states[i].position);
		player->SetDirection(game_state_.player_states[i].direction);
		i++;
	}
}

void ServerApplication::Update(float delta_time) {
	// Centre on the selected Player, Increase the size of the screen space to double.
	view_ = sf::View(sf::FloatRect(players_[selected_player_]->Position().x - static_cast<float>(window_.getSize().x),
								   players_[selected_player_]->Position().y - static_cast<float>(window_.getSize().y),
								   static_cast<float>(window_.getSize().x*2),
								   static_cast<float>(window_.getSize().y*2)));
	window_.setView(view_);

	for each (Player* player in players_) {		
		level_map_->CollisionCorrection(*player);
	}
}


void ServerApplication::RenderImGui(bool *open) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(200, 400);
	ImVec2 window_pos = ImVec2(
		window_.getSize().x - window_size.x,
		20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if (!ImGui::Begin("Game App", open, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}
	ImGui::Text("ID %d, %s", 
				game_state_.player_states[selected_player_].player_id,
				game_state_.player_states[selected_player_].username.c_str());
	
	ImGui::SliderInt("Follow", &selected_player_, 0, static_cast<int>(players_.size() - 1));	
	ImGui::End();
}
