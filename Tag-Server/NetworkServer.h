#ifndef _NETWORK_SERVER_H
#define _NETWORK_SERVER_H
#include "..\ImGui\imgui-SFML-console.h"

#include <vector>

#include "..\NetworkProtocol\Connection.h"
#include "..\NetworkProtocol\Messages.h"

#include "..\NetworkProtocol\Timer.h"

/// <summary>
/// Server for handling all clients.
/// Updates and manages connected clients, keeping them in sync and uptodate with server game state.
/// </summary>
class NetworkServer {
public:
	
	/// <summary>
	/// Generates the servers Player States, to be handed out to connecting clients.
	/// Set the server Tick Rate (Currently 30ms, or 33.3 ticks per second).
	/// TODO:: Expose server tick rate to ImGui for testing.
	/// </summary>
	NetworkServer();
	~NetworkServer();

	/// <summary>
	/// Handles proccessing recieved packets for that frame, and the gamestate update tick rate.
	/// </summary>
	/// <param name="delta_time"></param>
	void Update(sf::Time delta_time);

	/// <summary>
	/// Displays the Network state of all the players in a drop down menu in an ImGui window.
	/// </summary>
	/// <param name="open"> Allows the window to be closed with a X in top corner. 
	/// Determine to render based on the bool passed in. </param>
	void RenderImGui(bool *open);
	
	bool IsGameRunning();

	/// <summary>
	/// The current game state as view by the server.
	/// </summary>
	/// <returns> Refrence to the Server GameState. </returns>
	GameState& GetNetworkState();

	sf::Uint64 Frame();

private:
	/// <summary>
	/// Proccess recieved packets in a non-blocking manor.
	/// </summary>
	void ProcessRecievedPackets();

	/// <summary>
	/// Send GameState to all connected clients.
	/// </summary>
	void BroadcastGameStateUpdate();

	std::vector<Connection> clients_;
	GameState current_game_state_;

	sf::UdpSocket socket_;

	Timer server_tick_;

	ImGui::SFML::Console& console;
	sf::Uint64 frame_;
};

#endif
